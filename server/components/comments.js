/**
 * Created by hanni on 11/22/16.
 */
var MongoClient = require('mongodb').MongoClient;

exports.getAllComments = function getAllComments (req, res) {
    MongoClient.connect('mongodb://localhost:27017/comments')
        .then(response => {
            var comments = response.collection('comment').find().toArray();
            return comments;
            })
        .then(comments => {
            res.send(comments);
        })
};

exports.addNewComment = function addNewComment (req, res) {
    if (!req.body.author) {
        res.status(401).send('Author is mandatory');
    }
    if (!req.body.text) {
        res.status(401).send('Text is mandatory');
    }

    MongoClient.connect('mongodb://localhost:27017/comments')
        .then(response => {
            response.collection('comment').insert({"author": req.body.author, "text": req.body.text, "date": Date.now()});
            return true;
        })
        .then(send => {
            res.send('Added')   ;
        })

};